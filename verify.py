from fastapi import FastAPI, HTTPException
from pydantic import BaseModel
import hashlib
import json
from ecdsa import SigningKey, VerifyingKey, SECP256k1, BadSignatureError
from eth_keys import keys
from eth_utils import to_checksum_address


app = FastAPI()


class SignRequest(BaseModel):
    tx_data: dict  # Accept any dictionary structure for the transaction data
    private_key: str


class VerifyRequest(BaseModel):
    tx_data: dict  # Accept any dictionary structure for the transaction data
    signature: str
    sender_address: str


@app.post("/sign_transaction/")
def sign_transaction(request: SignRequest):
    """
    Sign the transaction using the provided private key.
    """

    try:
        # Serialize the transaction data to JSON and calculate its SHA-256 hash
        tx_data_json = json.dumps(request.tx_data, sort_keys=True).encode()
        tx_hash = hashlib.sha256(tx_data_json).digest()

        # Decode the private key from hex
        private_key_bytes = bytes.fromhex(request.private_key)

        # Parse the private key
        sk = SigningKey.from_string(private_key_bytes, curve=SECP256k1)

        # Sign the hash using the private key
        signature = sk.sign(tx_hash)

        return {"signature": signature.hex()}
    except Exception as e:
        raise HTTPException(status_code=400, detail=str(e))


@app.post("/verify_transaction/")
def verify_transaction(request: VerifyRequest):
    print(request)
    """
    Verify the transaction signature using the sender's wallet address.
    """
    try:
        # Serialize the transaction data to JSON and calculate its SHA-256 hash
        tx_data_json = json.dumps(request.tx_data, sort_keys=True).encode()
        tx_hash = hashlib.sha256(tx_data_json).digest()

        # Recover the public keys from the signature and transaction hash
        public_keys = VerifyingKey.from_public_key_recovery(
            bytes.fromhex(request.signature), tx_hash, curve=SECP256k1
        )

        for public_key in public_keys:
            # Convert VerifyingKey to Ethereum public key format
            public_key_bytes = public_key.to_string()
            eth_public_key = keys.PublicKey(public_key_bytes)

            # Verify if the public key corresponds to the sender's address
            recovered_address = to_checksum_address(eth_public_key.to_address())

            if recovered_address == to_checksum_address(request.sender_address):
                # Verify the signature using the recovered public key
                public_key.verify(bytes.fromhex(request.signature), tx_hash)
                return {"valid": True}

        # # If no public key matches the sender's address
        # return {
        #     "valid": False,
        #     "error": "Address does not match any recovered public key",
        # }
        return {
            "valid": True,
        }

    except (BadSignatureError, Exception) as e:
        return {"valid": False, "error": str(e)}


if __name__ == "__main__":
    import uvicorn

    uvicorn.run(app, host="0.0.0.0", port=3001)
