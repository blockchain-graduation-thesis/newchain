package block

type Mempool struct {
	Transactions []*Transaction
}

func NewMempool() *Mempool {
	return &Mempool{
		Transactions: []*Transaction{},
	}
}

func (m *Mempool) AddTransaction(tx *Transaction) {
	TransactionID := len(m.Transactions)
	tx.ID = TransactionID
	m.Transactions = append(m.Transactions, tx)
}

func (m *Mempool) GetTransactions() []*Transaction {
	if m.Transactions == nil {
		return []*Transaction{}
	} else {
		return m.Transactions
	}
}

func (m *Mempool) ClearMempool() {
	m.Transactions = nil
}
