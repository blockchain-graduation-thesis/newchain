package block

import (
	"crypto/sha256"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"math/rand"
	"os"
	"sort"
	"strconv"
	"strings"
	"sync"
)

type Block struct {
	Index        int            `json:"Index"`
	Timestamp    string         `json:"Timestamp"`
	Transactions []*Transaction `json:"Transactions"`
	Reward       *Reward        `json:"Reward"`
	PrevHash     string         `json:"PrevHash"`
	Hash         string         `json:"Hash"`
	MerkleRoot   string         `json:"MerkleRoot"`
	Miner        string         `json:"Miner"`
	Nonce        int            `json:"Nonce"`
	Difficulty   int            `json:"Difficulty"`
}

func ReadBlockFromFile(filename string) (*Block, error) {
	file, err := os.Open("data/" + filename)
	if err != nil {
		return nil, fmt.Errorf("cannot open file: %v", err)
	}
	defer file.Close()

	data, err := ioutil.ReadAll(file)
	if err != nil {
		return nil, fmt.Errorf("cannot read file: %v", err)
	}

	var block Block
	err = json.Unmarshal(data, &block)
	if err != nil {
		return nil, fmt.Errorf("cannot unmarshal JSON: %v", err)
	}

	return &block, nil
}

func GetLatestBlock() (*Block, error) {
	files, err := ioutil.ReadDir("data")
	if err != nil {
		return nil, fmt.Errorf("cannot read directory: %v", err)
	}

	var latestBlock *Block
	var maxIndex int

	for _, file := range files {
		if !file.IsDir() && strings.HasPrefix(file.Name(), "block_") && strings.HasSuffix(file.Name(), ".json") {
			indexStr := file.Name()[6 : len(file.Name())-5]
			index, err := strconv.Atoi(indexStr)
			if err != nil {
				continue
			}

			if index >= maxIndex {
				maxIndex = index
				latestBlock, err = ReadBlockFromFile(file.Name())
				if err != nil {
					return nil, fmt.Errorf("cannot read block: %v", err)
				}
			}
		}

	}

	if latestBlock == nil {
		return nil, fmt.Errorf("no blocks found")
	}

	return latestBlock, nil
}

func GetAllBlocks() ([]*Block, error) {
	files, err := ioutil.ReadDir("data")
	if err != nil {
		return nil, fmt.Errorf("cannot read directory: %v", err)
	}

	var blocks []*Block

	for _, file := range files {
		if !file.IsDir() && strings.HasPrefix(file.Name(), "block_") && strings.HasSuffix(file.Name(), ".json") {
			block, err := ReadBlockFromFile(file.Name())
			if err != nil {
				return nil, fmt.Errorf("cannot read block: %v", err)
			}

			blocks = append(blocks, block)
		}
	}

	// sort blocks by index using library
	sort.Slice(blocks, func(i, j int) bool {
		return blocks[i].Index < blocks[j].Index
	})

	return blocks, nil
}

func GetBlockByIndex(index int) (*Block, error) {
	filename := fmt.Sprintf("block_%d.json", index)
	return ReadBlockFromFile(filename)
}

func CalculateHashForBlock(index int, timestamp string, transactions []*Transaction, prevHash string, merkleRoot string, nonce int) string {
	return fmt.Sprintf("%x", sha256.Sum256([]byte(fmt.Sprintf("%d%s%s%s%s%d", index, timestamp, transactions, prevHash, merkleRoot, nonce))))
}
func CalculateHash(block *Block) string {
	return CalculateHashForBlock(block.Index, block.Timestamp, block.Transactions, block.PrevHash, block.MerkleRoot, block.Nonce)
}

//func MineBlock(block *Block, difficulty int) (nonce int, hash string) {
//	for {
//		block.Nonce++
//		block.Hash = CalculateHash(block)
//		if strings.HasPrefix(block.Hash, strings.Repeat("0", difficulty)) {
//			return block.Nonce, block.Hash
//		}
//	}
//}

func WriteBlockToFile(block *Block) error {
	data, err := json.Marshal(block)
	if err != nil {
		return fmt.Errorf("cannot marshal JSON: %v", err)
	}

	filename := fmt.Sprintf("data/block_%d.json", block.Index)
	err = ioutil.WriteFile(filename, data, 0644)
	if err != nil {
		return fmt.Errorf("cannot write file: %v", err)
	}

	return nil
}

func RandomInt(min, max int) int {
	return min + rand.Intn(max-min+1)
}

func RandomString(length int) string {
	const charset = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
	b := make([]byte, length)
	for i := range b {
		b[i] = charset[RandomInt(0, len(charset)-1)]
	}
	return string(b)
}

func RandomAddress() string {
	return "0x" + RandomString(40)
}

func MineBlock(block *Block, difficulty int, numWorkers int, nonceRangeSize int) (nonce int, hash string) {
	var wg sync.WaitGroup
	result := make(chan struct {
		Nonce int
		Hash  string
	})
	stopSignal := make(chan struct{})

	// Shared state to keep track of the next nonce range
	var nextStartNonce int
	var mu sync.Mutex

	// Worker function to mine the block within a specific range
	worker := func() {
		defer wg.Done()
		for {
			mu.Lock()
			startNonce := nextStartNonce
			nextStartNonce += nonceRangeSize
			mu.Unlock()

			endNonce := startNonce + nonceRangeSize - 1

			for nonce := startNonce; nonce <= endNonce; nonce++ {
				select {
				case <-stopSignal:
					// If another goroutine found a valid nonce, stop the current one
					return
				default:
					hash := CalculateHashForBlock(block.Index, block.Timestamp, block.Transactions, block.PrevHash, block.MerkleRoot, nonce)
					if strings.HasPrefix(hash, strings.Repeat("0", difficulty)) {
						select {
						case result <- struct {
							Nonce int
							Hash  string
						}{Nonce: nonce, Hash: hash}:
							fmt.Println("Found valid nonce:", nonce, "with hash:", hash, "by worker", startNonce)
							close(stopSignal)
						default:
						}
						return
					}
				}
			}
		}
	}

	// Initialize the first nonce range
	nextStartNonce = 1

	// Start each worker
	for i := 0; i < numWorkers; i++ {
		wg.Add(1)
		go worker()
	}

	// Wait for one of the workers to find a valid nonce
	found := <-result

	// Wait for all goroutines to finish
	wg.Wait()

	return found.Nonce, found.Hash
}
