package block

type DefaultTransaction struct {
	FROM   string `json:"FROM"`
	TO     string `json:"TO"`
	AMOUNT int    `json:"AMOUNT"`
}

type CreateToken struct {
	NAME    string `json:"NAME"`
	SYMBOL  string `json:"SYMBOL"`
	SUPPLY  int    `json:"SUPPLY"`
	ADDRESS string `json:"ADDRESS"`
	OWNER   string `json:"OWNER"`
}

type CreateNFToken struct {
	NAME    string `json:"NAME"`
	SYMBOL  string `json:"SYMBOL"`
	OWNER   string `json:"OWNER"`
	ADDRESS string `json:"ADDRESS"`
}

type MintNFToken struct {
	TO      string `json:"TO"`
	ID      int    `json:"ID"`
	ADDRESS string `json:"ADDRESS"`
}

type TransactionToken struct {
	FROM    string `json:"FROM"`
	TO      string `json:"TO"`
	AMOUNT  int    `json:"AMOUNT"`
	ADDRESS string `json:"ADDRESS"`
}

type TransferNFToken struct {
	FROM    string `json:"FROM"`
	TO      string `json:"TO"`
	ADDRESS string `json:"ADDRESS"`
	ID      int    `json:"ID"`
}

type BurnToken struct {
	FROM    string `json:"FROM"`
	ADDRESS string `json:"ADDRESS"`
	AMOUNT  int    `json:"AMOUNT"`
}

type BurnNFToken struct {
	FROM    string `json:"FROM"`
	ADDRESS string `json:"ADDRESS"`
	ID      int    `json:"ID"`
}

type Token struct {
	NAME   string `json:"NAME"`
	SYMBOL string `json:"SYMBOL"`
	SUPPLY int    `json:"SUPPLY"`
	OWNER  string `json:"OWNER"`
}

type NFToken struct {
	NAME   string `json:"NAME"`
	SYMBOL string `json:"SYMBOL"`
	OWNER  string `json:"OWNER"`
}

type Reward struct {
	FROM   string `json:"FROM"`
	TO     string `json:"TO"`
	AMOUNT int    `json:"AMOUNT"`
}

// Transaction struct
type Transaction struct {
	ID        int         `json:"ID"`
	FROM      string      `json:"FROM"`
	TxData    interface{} `json:"TxData"`
	Signature string      `json:"Signature"`
}

func CreateTransaction(txData interface{}, signature string, from string) (*Transaction, error) {
	tx := &Transaction{
		ID:        0,
		TxData:    txData,
		Signature: signature,
		FROM:      from,
	}
	return tx, nil
}
