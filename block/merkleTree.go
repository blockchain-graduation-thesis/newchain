package block

import (
	"crypto/sha256"
	"encoding/hex"
	"encoding/json"
)

func CalculateMerkleRoot(transactions []*Transaction) string {

	if len(transactions) == 0 {
		return hex.EncodeToString(sha256.New().Sum(nil))
	}

	var hashes []string
	for _, tx := range transactions {
		txBytes, err := json.Marshal(tx)
		if err != nil {
			continue // handle error as needed
		}
		hash := sha256.Sum256(txBytes)
		hashes = append(hashes, hex.EncodeToString(hash[:]))
	}

	for len(hashes) > 1 {
		if len(hashes)%2 != 0 {
			hashes = append(hashes, hashes[len(hashes)-1])
		}

		var newLevel []string
		for i := 0; i < len(hashes); i += 2 {
			newHash := sha256.Sum256([]byte(hashes[i] + hashes[i+1]))
			newLevel = append(newLevel, hex.EncodeToString(newHash[:]))
		}
		hashes = newLevel
	}

	return hashes[0]
}
