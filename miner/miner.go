package miner

import (
	"fmt"
	"net"
	"sync"
)

type Miner struct {
	Ip      string
	Address string
}

var (
	MinerList  []Miner
	MinerMutex sync.Mutex
	LocalPort  string
)

func ContainsMiner(ip string) bool {
	for _, miner := range MinerList {
		if miner.Ip == ip {
			return true
		}
	}
	return false
}

func GetIp() string {
	addrs, err := net.InterfaceAddrs()
	if err != nil {
		fmt.Println(err)
	}
	for _, address := range addrs {
		if ipnet, ok := address.(*net.IPNet); ok && !ipnet.IP.IsLoopback() {
			if ipnet.IP.To4() != nil {
				return ipnet.IP.String()
			}
		}
	}
	return ""
}

func RemoveMiner(address string) {
	MinerMutex.Lock()
	defer MinerMutex.Unlock()
	for i, miner := range MinerList {
		if miner.Address == address {
			MinerList = append(MinerList[:i], MinerList[i+1:]...)
			return
		}
	}
}

func GetMinerList() []Miner {
	return MinerList
}
