package blockchain

import (
	"fmt"
	"math/big"
	"newchain/block"
	bl "newchain/block"
	pb "newchain/gen/go"
	"time"
)

var GlobalMempool *block.Mempool

func init() {
	if GlobalMempool == nil {
		GlobalMempool = bl.NewMempool()
	}
}

func GetTransactions() []*bl.Transaction {
	return GlobalMempool.GetTransactions()
}

func AddTransaction(tx *bl.Transaction) {
	GlobalMempool.AddTransaction(tx)

}

func ConvertStringToTime(str string) time.Time {
	t, _ := time.Parse(time.RFC3339, str)
	return t
}

func VerifyBlock(block *bl.Block) bool {
	// calculate merkle root
	merkleRoot := bl.CalculateMerkleRoot(block.Transactions)
	if merkleRoot != block.MerkleRoot {
		return false
	}

	//// verify transactions
	//for _, tx := range block.Transactions {
	//	txData, ok := tx.TxData.(map[string]interface{})
	//	if !ok {
	//		fmt.Println("Error asserting TxData to map")
	//		return false
	//	}
	//
	//	from, ok := txData["FROM"].(string)
	//	if !ok {
	//		fmt.Println("Error asserting FROM")
	//		return false
	//	}
	//
	//	publicKey, err := bl.ConvertAddressToPublicKey(from)
	//	if err != nil {
	//		return false
	//	}

	//if !bl.VerifyTransaction(tx, publicKey) {
	//	return false
	//}

	//if !tx.VerifyTransaction(publicKey) {
	//	return false
	//}
	//}

	// calculate hash
	hash := bl.CalculateHash(block)
	if hash != block.Hash {
		return false
	}

	return true
}

func AddBlock(block *bl.Block) error {
	err := bl.WriteBlockToFile(block)
	if err != nil {
		return err
	}
	return nil
}

func CreateBlock(block *bl.Block) (bl.Block, error) {

	// get wallet from .env file

	// wallet := os.Getenv("WALLET")
	wallet := "0xb2c4E0862c9252F9a1bD712e329fD13Cfc7A7a15"
	// create new block

	newBlock := &bl.Block{
		Index:        block.Index + 1,
		Timestamp:    time.Now().Format(time.RFC3339),
		Transactions: GlobalMempool.GetTransactions(),
		MerkleRoot:   bl.CalculateMerkleRoot(GlobalMempool.GetTransactions()),
		Reward:       &bl.Reward{FROM: "0x000000000000000000000000000000000000000000", TO: wallet, AMOUNT: 1},
		PrevHash:     block.Hash,
		Miner:        wallet,
	}

	// clear mempool
	GlobalMempool.ClearMempool()

	// calculate difficulty of mining (difficulty = difficulty + 1 after every 100 blocks)
	if newBlock.Index%1000 == 0 {
		newBlock.Difficulty = block.Difficulty + 1
	} else {
		newBlock.Difficulty = block.Difficulty
	}

	// mine block
	Nonce, Hash := bl.MineBlock(newBlock, newBlock.Difficulty, 10, 1000)

	newBlock.Nonce = Nonce
	newBlock.Hash = Hash

	return *newBlock, nil

}

func CheckBlockCreation() (bl.Block, error) {
	block, err := bl.GetLatestBlock()
	if err != nil {
		fmt.Println("Error getting latest block: ", err)
		return bl.Block{}, err
	}

	if time.Now().Sub(ConvertStringToTime(block.Timestamp)) > 5*time.Second {
		block, err := CreateBlock(block)
		if err != nil {
			return bl.Block{}, err
		}

		if block.Index != 0 {
			err := AddBlock(&block)
			if err != nil {
				return bl.Block{}, err
			}
		}
	}

	transactions := []*pb.Transaction{}
	for _, tx := range block.Transactions {
		var pbTx *pb.Transaction

		switch txData := tx.TxData.(type) {
		case *bl.DefaultTransaction:
			pbTx = &pb.Transaction{
				Data: &pb.InfoTransaction{
					TxData: &pb.InfoTransaction_DefaultTx{
						DefaultTx: &pb.DefaultTransaction{
							From:   txData.FROM,
							To:     txData.TO,
							Amount: int64(txData.AMOUNT),
						},
					},
					Signature: tx.Signature,
				},
			}
		case *bl.CreateToken:
			pbTx = &pb.Transaction{
				Data: &pb.InfoTransaction{
					TxData: &pb.InfoTransaction_CreateTokenTx{
						CreateTokenTx: &pb.CreateToken{
							Name:    txData.NAME,
							Symbol:  txData.SYMBOL,
							Supply:  int64(txData.SUPPLY),
							Address: txData.ADDRESS,
							Owner:   txData.OWNER,
						},
					},
					Signature: tx.Signature,
				},
			}
		case *bl.CreateNFToken:
			pbTx = &pb.Transaction{
				Data: &pb.InfoTransaction{
					TxData: &pb.InfoTransaction_CreateNfTokenTx{
						CreateNfTokenTx: &pb.CreateNFToken{
							Name:    txData.NAME,
							Symbol:  txData.SYMBOL,
							Owner:   txData.OWNER,
							Address: txData.ADDRESS,
						},
					},
					Signature: tx.Signature,
				},
			}
		case *bl.MintNFToken:
			pbTx = &pb.Transaction{
				Data: &pb.InfoTransaction{
					TxData: &pb.InfoTransaction_MintNfTokenTx{
						MintNfTokenTx: &pb.MintNFToken{
							To:      txData.TO,
							Id:      int64(txData.ID),
							Address: txData.ADDRESS,
						},
					},
					Signature: tx.Signature,
				},
			}
		case *bl.TransactionToken:
			pbTx = &pb.Transaction{
				Data: &pb.InfoTransaction{
					TxData: &pb.InfoTransaction_TransactionTokenTx{
						TransactionTokenTx: &pb.TransactionToken{
							From:    txData.FROM,
							To:      txData.TO,
							Amount:  int64(txData.AMOUNT),
							Address: txData.ADDRESS,
						},
					},
					Signature: tx.Signature,
				},
			}
		case *bl.TransferNFToken:
			pbTx = &pb.Transaction{
				Data: &pb.InfoTransaction{
					TxData: &pb.InfoTransaction_TransferNfTokenTx{
						TransferNfTokenTx: &pb.TransferNFToken{
							From:    txData.FROM,
							To:      txData.TO,
							Address: txData.ADDRESS,
							Id:      int64(txData.ID),
						},
					},
					Signature: tx.Signature,
				},
			}
		case *bl.BurnToken:
			pbTx = &pb.Transaction{
				Data: &pb.InfoTransaction{
					TxData: &pb.InfoTransaction_BurnTokenTx{
						BurnTokenTx: &pb.BurnToken{
							From:    txData.FROM,
							Address: txData.ADDRESS,
							Amount:  int64(txData.AMOUNT),
						},
					},
					Signature: tx.Signature,
				},
			}
		case *bl.BurnNFToken:
			pbTx = &pb.Transaction{
				Data: &pb.InfoTransaction{
					TxData: &pb.InfoTransaction_BurnNfTokenTx{
						BurnNfTokenTx: &pb.BurnNFToken{
							From:    txData.FROM,
							Address: txData.ADDRESS,
							Id:      int64(txData.ID),
						},
					},
					Signature: tx.Signature,
				},
			}
		default:
			continue
		}

		transactions = append(transactions, pbTx)
	}

	//go mn.ShareNewBlock(&pb.NewBlockRequest{
	//	Block: &pb.Block{
	//		Index:        int64(block.Index),
	//		Timestamp:    block.Timestamp,
	//		Transactions: transactions,
	//		PreviousHash: block.PrevHash,
	//		MerkleRoot:   block.MerkleRoot,
	//		Nonce:        int64(block.Nonce),
	//		Hash:         block.Hash,
	//		Miner:        block.Miner,
	//		Difficulty:   int64(block.Difficulty),
	//	},
	//})

	return bl.Block{}, nil
}

func GetBalance(wallet string) int {
	var balance float64
	balance = 0

	// get all blocks
	blocks, err := bl.GetAllBlocks()
	if err != nil {
		fmt.Println("Error getting blocks: ", err)
		return 0
	}

	// iterate over all blocks
	for _, block := range blocks {
		// iterate over all transactions in a block
		for _, tx := range block.Transactions {
			txData, ok := tx.TxData.(map[string]interface{})
			if ok {
				if txData["ADDRESS"] == nil {
					if txData["FROM"] == wallet {
						balance -= txData["AMOUNT"].(float64)
					}
					if txData["TO"] == wallet {
						balance += txData["AMOUNT"].(float64)
					}
				}
			}
		}
		if block.Reward != nil {
			if block.Reward.TO == wallet {
				balance += float64(block.Reward.AMOUNT)
			}
		}

	}

	return int(balance)
}

func GetTokenBalance(wallet, tokenAddress string) int {
	var balance float64
	balance = 0
	// get all blocks
	blocks, err := bl.GetAllBlocks()
	if err != nil {
		fmt.Println("Error getting blocks: ", err)
		return 0
	}

	// iterate over all blocks
	for _, block := range blocks {
		// iterate over all transactions in a block
		for _, tx := range block.Transactions {
			txData, ok := tx.TxData.(map[string]interface{})
			fmt.Println(txData)
			if !ok {
				fmt.Println("Error asserting TxData to map")
				continue
			}

			// check if the transaction is a token transaction
			if txData["ADDRESS"] == tokenAddress {
				if txData["FROM"] == wallet {
					balance -= txData["AMOUNT"].(float64)
				}
				if txData["TO"] == wallet {
					balance += txData["AMOUNT"].(float64)
				}
				if txData["OWNER"] == wallet {
					balance += txData["SUPPLY"].(float64)
				}
			}
		}
	}

	return int(balance)
}

// return list id
func GetNFTBalance(wallet, tokenAddress string) []int64 {
	var balance []int64
	balance = []int64{}
	// get all blocks
	blocks, err := bl.GetAllBlocks()
	if err != nil {
		fmt.Println("Error getting blocks: ", err)
		return balance
	}

	// iterate over all blocks
	for _, block := range blocks {
		// iterate over all transactions in a block
		for _, tx := range block.Transactions {
			txData, ok := tx.TxData.(map[string]interface{})
			if !ok {
				fmt.Println("Error asserting TxData to map")
				continue
			}

			// check if the transaction is a token transaction
			if txData["ADDRESS"] == tokenAddress {
				if txData["TO"] == wallet {
					balance = append(balance, int64(txData["ID"].(float64)))
				}
				if txData["FROM"] == wallet {
					for i, id := range balance {
						if id == int64(txData["ID"].(float64)) {
							balance = append(balance[:i], balance[i+1:]...)
						}
					}
				}
			}
		}
	}

	return balance
}

// check nftID is valid
func CheckNFTId(tokenAddress string, nftID int64) bool {
	// get all blocks
	blocks, err := bl.GetAllBlocks()
	if err != nil {
		fmt.Println("Error getting blocks: ", err)
		return false
	}
	fmt.Println("blocks")

	// iterate over all blocks
	for _, block := range blocks {
		// iterate over all transactions in a block
		for _, tx := range block.Transactions {
			txData, ok := tx.TxData.(map[string]interface{})
			if !ok {
				fmt.Println("Error asserting TxData to map")
				continue
			}

			// check if the transaction is a token transaction
			if txData["ADDRESS"] == tokenAddress {
				if (txData["ID"] != nil) && (txData["ID"].(float64) == float64(nftID)) {
					return true
				}
			}
		}
	}

	return false
}

func GetNFTOwner(tokenAddress string, nftID int64) string {
	// get all blocks
	blocks, err := bl.GetAllBlocks()
	if err != nil {
		fmt.Println("Error getting blocks: ", err)
		return ""
	}

	var owner string

	// iterate over all blocks
	for _, block := range blocks {
		// iterate over all transactions in a block
		for _, tx := range block.Transactions {
			txData, ok := tx.TxData.(map[string]interface{})
			if !ok {
				fmt.Println("Error asserting TxData to map")
				continue
			}
			// check if the transaction is a token transaction
			if txData["ADDRESS"] == tokenAddress {
				if txData["ID"] != nil && txData["ID"].(float64) == float64(nftID) {
					owner = txData["TO"].(string)
				}
			}

		}
	}
	return owner
}

func GetNFTInfo(tokenAddress string) bl.NFToken {
	// get all blocks
	blocks, err := bl.GetAllBlocks()
	if err != nil {
		fmt.Println("Error getting blocks: ", err)
		return bl.NFToken{}
	}

	var nft bl.NFToken

	// iterate over all blocks
	for _, block := range blocks {
		// iterate over all transactions in a block
		for _, tx := range block.Transactions {
			txData, ok := tx.TxData.(map[string]interface{})
			if !ok {
				fmt.Println("Error asserting TxData to map")
				continue
			}
			// check if the transaction is a token transaction
			if txData["ADDRESS"] == tokenAddress {
				if txData["NAME"] != nil {
					nft.NAME = txData["NAME"].(string)
				}
				if txData["SYMBOL"] != nil {
					nft.SYMBOL = txData["SYMBOL"].(string)
				}
			}

		}
	}
	return nft
}

func GetTokenInfo(tokenAddress string) bl.Token {
	// get all blocks
	blocks, err := bl.GetAllBlocks()
	if err != nil {
		fmt.Println("Error getting blocks: ", err)
		return bl.Token{}
	}

	var token bl.Token

	// iterate over all blocks
	for _, block := range blocks {
		// iterate over all transactions in a block
		for _, tx := range block.Transactions {
			txData, ok := tx.TxData.(map[string]interface{})
			if !ok {
				fmt.Println("Error asserting TxData to map")
				continue
			}
			// check if the transaction is a token transaction
			if txData["ADDRESS"] == tokenAddress {
				if txData["NAME"] != nil {
					token.NAME = txData["NAME"].(string)
				}
				if txData["SYMBOL"] != nil {
					token.SYMBOL = txData["SYMBOL"].(string)
				}
				if txData["SUPPLY"] != nil {
					token.SUPPLY = int(txData["SUPPLY"].(float64))
				}
				if txData["OWNER"] != nil {
					token.OWNER = txData["OWNER"].(string)
				}
			}

		}

	}
	return token

}

// CalculateGas calculates the gas cost for a transaction in Wei
func CalculateGas(dataSize int, mempoolSize int) *big.Int {
	const (
		BaseGasLimit       = 21000 // Base gas limit for a standard transaction
		GasPrice           = 50    // Gas price in Gwei (example value)
		GasPerDataByte     = 68    // Gas cost per byte of data in the transaction
		AdditionalGasPerTx = 1000  // Additional gas per transaction in mempool
	)

	gasLimit := BaseGasLimit + GasPerDataByte*dataSize + AdditionalGasPerTx*mempoolSize
	gasPriceInWei := big.NewInt(GasPrice * 1e9) // Convert GasPrice from Gwei to Wei

	gasCost := big.NewInt(0).Mul(big.NewInt(int64(gasLimit)), gasPriceInWei)
	return gasCost
}

func ConvertWeiToEther(wei *big.Int) *big.Float {
	weiFloat := new(big.Float).SetInt(wei)
	etherValue := new(big.Float).Quo(weiFloat, big.NewFloat(1e18))
	return etherValue
}

func GetBlocksByPage(page int, pageSize int) ([]*bl.Block, error) {
	if page < 1 {
		return nil, fmt.Errorf("page must be greater than 0")
	}
	if pageSize < 1 {
		return nil, fmt.Errorf("pageSize must be greater than 0")
	}

	blocks, err := bl.GetAllBlocks()
	// check page  * pageSize > len(blocks)
	if (page-1)*pageSize > len(blocks) {
		return nil, fmt.Errorf("page out of range")
	}

	if err != nil {
		return nil, err
	}

	start := (page - 1) * pageSize
	end := (page-1)*pageSize + pageSize
	if end > len(blocks) {
		end = len(blocks)
	}

	return blocks[start:end], nil
}
