package main

import (
	"newchain/blockchain"
	"newchain/gRPC"
)

func main() {

	go func() {
		for {
			blockchain.CheckBlockCreation()
		}
	}()

	gRPC.GRPC()
}
