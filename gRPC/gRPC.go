package gRPC

import (
	"bytes"
	"context"
	"crypto/sha256"
	"encoding/hex"
	"encoding/json"
	"fmt"
	"log"
	"net"
	"net/http"
	"sync"

	"google.golang.org/genproto/googleapis/api/httpbody"

	bl "newchain/block"
	bc "newchain/blockchain"
	pb "newchain/gen/go"
	mn "newchain/miner"

	"google.golang.org/grpc"
)

var (
	processedTransactions      = make(map[string]bool)
	processedTransactionsMutex sync.Mutex
)

type server struct {
	pb.UnimplementedNodeServiceServer
}

func convertMinerListToPb(miners []mn.Miner) []*pb.Miner {
	var pbMiners []*pb.Miner
	for _, miner := range miners {
		pbMiners = append(pbMiners, &pb.Miner{Ip: miner.Ip, Address: miner.Address})
	}
	return pbMiners
}

func (s *server) Connect(ctx context.Context, in *pb.ConnectRequest) (*pb.ConnectResponse, error) {
	log.Printf("Received: %v", in.GetIp())

	mn.MinerMutex.Lock()
	defer mn.MinerMutex.Unlock()

	if !mn.ContainsMiner(in.GetIp()) {

		mn.MinerList = append(mn.MinerList, mn.Miner{Ip: in.GetIp(), Address: in.GetAddress()})
		return &pb.ConnectResponse{
			Success: true,
			Miners:  convertMinerListToPb(mn.MinerList),
		}, nil
	} else {
		return &pb.ConnectResponse{
			Success: false,
		}, nil
	}
}

func generateTransactionID(txData interface{}) string {
	var data string

	// convert txData to string
	switch tx := txData.(type) {
	case *pb.NewDefaultTransactionRequest:
		data = fmt.Sprintf("%s-%s-%d-%s", tx.GetTransaction().GetFrom(), tx.GetTransaction().GetTo(), tx.GetTransaction().GetAmount(), tx.GetSignature())
	case *pb.CreateTokenRequest:
		data = fmt.Sprintf("%s-%s-%d-%s", tx.GetTransaction().GetOwner(), tx.GetTransaction().GetName(), tx.GetTransaction().GetSupply(), tx.GetSignature())
	case *pb.CreateNFTokenRequest:
		data = fmt.Sprintf("%s-%s-%s-%s", tx.GetTransaction().GetOwner(), tx.GetTransaction().GetName(), tx.GetTransaction().GetSymbol(), tx.GetSignature())
	case *pb.MintNFTokenRequest:
		data = fmt.Sprintf("%s-%s-%d-%s", tx.GetTransaction().GetTo(), tx.GetTransaction().GetAddress(), tx.GetTransaction().GetId(), tx.GetSignature())
	case *pb.TransactionTokenRequest:
		data = fmt.Sprintf("%s-%s-%d-%s", tx.GetTransaction().GetFrom(), tx.GetTransaction().GetTo(), tx.GetTransaction().GetAmount(), tx.GetSignature())
	case *pb.TransferNFTokenRequest:
		data = fmt.Sprintf("%s-%s-%d-%s", tx.GetTransaction().GetFrom(), tx.GetTransaction().GetTo(), tx.GetTransaction().GetId(), tx.GetSignature())
	case *pb.BurnTokenRequest:
		data = fmt.Sprintf("%s-%s-%d-%s", tx.GetTransaction().GetFrom(), tx.GetTransaction().GetAddress(), tx.GetTransaction().GetAmount(), tx.GetSignature())
	case *pb.BurnNFTokenRequest:
		data = fmt.Sprintf("%s-%s-%d-%s", tx.GetTransaction().GetFrom(), tx.GetTransaction().GetAddress(), tx.GetTransaction().GetId(), tx.GetSignature())
	}
	hash := sha256.Sum256([]byte(data))
	return hex.EncodeToString(hash[:])
}

func (s *server) NewDefaultTransaction(ctx context.Context, in *pb.NewDefaultTransactionRequest) (*pb.NewDefaultTransactionResponse, error) {
	var txData interface{}
	var err error
	fmt.Println("Received new transaction: ", in)

	if in.GetTransaction().GetAmount() < 0 {
		return &pb.NewDefaultTransactionResponse{Success: false}, fmt.Errorf("amount cannot be negative")
	} else if bc.GetBalance(in.From) < int(in.GetTransaction().GetAmount()) {
		return &pb.NewDefaultTransactionResponse{Success: false}, fmt.Errorf("insufficient funds")
	}
	txData = &bl.DefaultTransaction{
		FROM:   in.GetTransaction().GetFrom(),
		TO:     in.GetTransaction().GetTo(),
		AMOUNT: int(in.GetTransaction().GetAmount()),
	}

	// Encode transaction data to JSON to calculate dataSize
	txDataBytes, err := json.Marshal(txData)
	if err != nil {
		return &pb.NewDefaultTransactionResponse{Success: false}, fmt.Errorf("error encoding transaction data: %v", err)
	}
	dataSize := len(txDataBytes)

	gasCost := bc.CalculateGas(dataSize, len(bc.GetTransactions()))

	fmt.Println("Gas cost: ", bc.ConvertWeiToEther(gasCost).String())

	newTx, err := bl.CreateTransaction(txData, in.GetSignature(), in.GetFrom())
	if err != nil {
		log.Println("Error creating transaction: ", err)
		return &pb.NewDefaultTransactionResponse{Success: false}, nil
	}

	bc.AddTransaction(newTx) // Add the transaction to the mempool
	fmt.Println("All transactions: ", bc.GetTransactions())
	processedTransactionsMutex.Lock()
	processedTransactions[generateTransactionID(in)] = true
	processedTransactionsMutex.Unlock()

	if processedTransactions[generateTransactionID(in)] {
		return &pb.NewDefaultTransactionResponse{Success: true}, nil
	}

	go func() {
		// call newbTransaction function to share the transaction with other miners

	}() // this is a goroutine

	return &pb.NewDefaultTransactionResponse{Success: true}, nil
}

// verifyTransactionWithPython calls the Python API to verify a transaction
func verifyTransactionWithPython(txData interface{}, signature string, senderAddress string) (*VerifyResponse, error) {
	url := "http://54.169.241.252:3001/verify_transaction/"
	fmt.Println("Verifying transaction with Python API...")
	reqBody, err := json.Marshal(map[string]interface{}{
		"tx_data":        txData,
		"signature":      signature,
		"sender_address": senderAddress,
	})
	if err != nil {
		return nil, fmt.Errorf("error marshaling request body: %v", err)
	}

	resp, err := http.Post(url, "application/json", bytes.NewBuffer(reqBody))
	fmt.Println("Response: ", resp)
	if err != nil {
		return nil, fmt.Errorf("error making POST request: %v", err)
	}
	defer resp.Body.Close()

	var verifyResponse VerifyResponse
	if err := json.NewDecoder(resp.Body).Decode(&verifyResponse); err != nil {
		return nil, fmt.Errorf("error decoding response body: %v", err)
	}

	return &verifyResponse, nil
}

type VerifyResponse struct {
	Valid bool   `json:"valid"`
	Error string `json:"error,omitempty"`
}

func (s *server) CreateToken(ctx context.Context, in *pb.CreateTokenRequest) (*pb.CreateTokenResponse, error) {
	var txData interface{}
	var err error

	txData = &bl.CreateToken{
		NAME:   in.GetTransaction().GetName(),
		SYMBOL: in.GetTransaction().GetSymbol(),
		SUPPLY: int(in.GetTransaction().GetSupply()),
		OWNER:  in.GetTransaction().GetOwner(),
	}
	if in.GetTransaction().GetAddress() == "" {
		txData.(*bl.CreateToken).ADDRESS = bl.RandomAddress()
	} else {
		txData.(*bl.CreateToken).ADDRESS = in.GetTransaction().GetAddress()
	}

	txDataBytes, err := json.Marshal(txData)
	if err != nil {
		return &pb.CreateTokenResponse{Success: false}, fmt.Errorf("error encoding transaction data: %v", err)
	}
	dataSize := len(txDataBytes)

	gasCost := bc.CalculateGas(dataSize, len(bc.GetTransactions()))

	fmt.Println("Gas cost: ", bc.ConvertWeiToEther(gasCost).String())

	newTx, err := bl.CreateTransaction(txData, in.GetSignature(), in.GetTxFrom())
	if err != nil {
		log.Println("Error creating transaction: ", err)
		return &pb.CreateTokenResponse{Success: false}, nil
	}

	verificationResult, err := verifyTransactionWithPython(txData, in.GetSignature(), in.GetTxFrom())
	if err != nil {
		log.Println("Error verifying transaction: ", err)
		return &pb.CreateTokenResponse{Success: false}, nil
	}

	if !verificationResult.Valid {
		return &pb.CreateTokenResponse{Success: false}, fmt.Errorf("transaction verification failed: %v", verificationResult.Error)
	}

	bc.AddTransaction(newTx)
	fmt.Println("All transactions: ", bc.GetTransactions())
	processedTransactionsMutex.Lock()
	processedTransactions[generateTransactionID(in)] = true
	processedTransactionsMutex.Unlock()

	if processedTransactions[generateTransactionID(in)] {
		return &pb.CreateTokenResponse{Success: true}, nil
	}

	go func() {
		// call newbTransaction function to share the transaction with other miners
		//
	}() // this is a goroutine

	return &pb.CreateTokenResponse{Success: true}, nil
}

func (s *server) CreateNFToken(ctx context.Context, in *pb.CreateNFTokenRequest) (*pb.CreateNFTokenResponse, error) {
	var txData interface{}
	var err error
	fmt.Println("Received new transaction: ", in)

	txData = &bl.CreateNFToken{
		NAME:   in.GetTransaction().GetName(),
		SYMBOL: in.GetTransaction().GetSymbol(),
		OWNER:  in.GetTransaction().GetOwner(),
	}
	if in.GetTransaction().GetAddress() == "" {
		txData.(*bl.CreateNFToken).ADDRESS = bl.RandomAddress()
	} else {
		txData.(*bl.CreateNFToken).ADDRESS = in.GetTransaction().GetAddress()
	}

	// Encode transaction data to JSON to calculate dataSize
	txDataBytes, err := json.Marshal(txData)
	if err != nil {
		return &pb.CreateNFTokenResponse{Success: false}, fmt.Errorf("error encoding transaction data: %v", err)
	}
	dataSize := len(txDataBytes)

	gasCost := bc.CalculateGas(dataSize, len(bc.GetTransactions()))

	fmt.Println("Gas cost: ", bc.ConvertWeiToEther(gasCost).String())

	newTx, err := bl.CreateTransaction(txData, in.GetSignature(), in.GetTxFrom())
	if err != nil {
		log.Println("Error creating transaction: ", err)
		return &pb.CreateNFTokenResponse{Success: false}, nil
	}

	verificationResult, err := verifyTransactionWithPython(txData, in.GetSignature(), in.GetTxFrom())
	if err != nil {
		log.Println("Error verifying transaction: ", err)
		return &pb.CreateNFTokenResponse{Success: false}, nil
	}

	if !verificationResult.Valid {
		return &pb.CreateNFTokenResponse{Success: false}, fmt.Errorf("transaction verification failed: %v", verificationResult.Error)
	}

	bc.AddTransaction(newTx) // Add the transaction to the mempool
	fmt.Println("All transactions: ", bc.GetTransactions())
	processedTransactionsMutex.Lock()
	processedTransactions[generateTransactionID(in)] = true
	processedTransactionsMutex.Unlock()

	if processedTransactions[generateTransactionID(in)] {
		return &pb.CreateNFTokenResponse{Success: true}, nil
	}

	go func() {
		// call newbTransaction function to share the transaction with other miners
		//
	}() // this is a goroutine

	return &pb.CreateNFTokenResponse{Success: true}, nil
}

func (s *server) MintNFToken(ctx context.Context, in *pb.MintNFTokenRequest) (*pb.MintNFTokenResponse, error) {
	var txData interface{}
	var err error
	fmt.Println("Transaction: ", in.GetTransaction().GetId())
	if bc.CheckNFTId(in.GetTransaction().GetAddress(), in.GetTransaction().GetId()) {
		return &pb.MintNFTokenResponse{Success: false}, fmt.Errorf("NFT ID already exists")
	}
	fmt.Println("NFT ID does not exist")

	txData = &bl.MintNFToken{
		TO:      in.GetTransaction().GetTo(),
		ID:      int(in.GetTransaction().GetId()),
		ADDRESS: in.GetTransaction().GetAddress(),
	}

	// Encode transaction data to JSON to calculate dataSize
	txDataBytes, err := json.Marshal(txData)
	if err != nil {
		return &pb.MintNFTokenResponse{Success: false}, fmt.Errorf("error encoding transaction data: %v", err)
	}
	dataSize := len(txDataBytes)

	gasCost := bc.CalculateGas(dataSize, len(bc.GetTransactions()))

	fmt.Println("Gas cost: ", bc.ConvertWeiToEther(gasCost).String())

	newTx, err := bl.CreateTransaction(txData, in.GetSignature(), in.GetTxFrom())
	if err != nil {
		log.Println("Error creating transaction: ", err)
		return &pb.MintNFTokenResponse{Success: false}, nil
	}

	verificationResult, err := verifyTransactionWithPython(txData, in.GetSignature(), in.GetTxFrom())
	if err != nil {
		log.Println("Error verifying transaction: ", err)
		return &pb.MintNFTokenResponse{Success: false}, nil
	}

	if !verificationResult.Valid {
		return &pb.MintNFTokenResponse{Success: false}, fmt.Errorf("transaction verification failed: %v", verificationResult.Error)
	}

	bc.AddTransaction(newTx) // Add the transaction to the mempool
	fmt.Println("All transactions: ", bc.GetTransactions())
	processedTransactionsMutex.Lock()
	processedTransactions[generateTransactionID(in)] = true
	processedTransactionsMutex.Unlock()

	if processedTransactions[generateTransactionID(in)] {
		return &pb.MintNFTokenResponse{Success: true}, nil
	}

	go func() {
		// call newbTransaction function to share the transaction with other miners
		//
	}() // this is a goroutine

	return &pb.MintNFTokenResponse{Success: true}, nil
}

func (s *server) TransactionToken(ctx context.Context, in *pb.TransactionTokenRequest) (*pb.TransactionTokenResponse, error) {
	var txData interface{}
	var err error
	fmt.Println("Received new transaction: ", in)

	if in.GetTransaction().GetAmount() < 0 {
		log.Println("Amount cannot be negative")
		return &pb.TransactionTokenResponse{Success: false}, fmt.Errorf("amount cannot be negative")
	} else if bc.GetTokenBalance(in.GetTransaction().GetFrom(), in.GetTransaction().GetAddress()) < int(in.GetTransaction().GetAmount()) {
		log.Println("Insufficient funds")
		return &pb.TransactionTokenResponse{Success: false}, fmt.Errorf("insufficient funds")
	}
	txData = &bl.TransactionToken{
		FROM:    in.GetTransaction().GetFrom(),
		TO:      in.GetTransaction().GetTo(),
		AMOUNT:  int(in.GetTransaction().GetAmount()),
		ADDRESS: in.GetTransaction().GetAddress(),
	}

	// Encode transaction data to JSON to calculate dataSize
	txDataBytes, err := json.Marshal(txData)
	if err != nil {
		return &pb.TransactionTokenResponse{Success: false}, fmt.Errorf("error encoding transaction data: %v", err)
	}
	dataSize := len(txDataBytes)

	gasCost := bc.CalculateGas(dataSize, len(bc.GetTransactions()))

	fmt.Println("Gas cost: ", bc.ConvertWeiToEther(gasCost).String())

	newTx, err := bl.CreateTransaction(txData, in.GetSignature(), in.GetTxFrom())
	if err != nil {
		log.Println("Error creating transaction: ", err)
		return &pb.TransactionTokenResponse{Success: false}, nil
	}
	verificationResult, err := verifyTransactionWithPython(txData, in.GetSignature(), in.GetTxFrom())
	if err != nil {
		log.Println("Error verifying transaction: ", err)
		return &pb.TransactionTokenResponse{Success: false}, nil
	}

	if !verificationResult.Valid {
		return &pb.TransactionTokenResponse{Success: false}, fmt.Errorf("transaction verification failed: %v", verificationResult.Error)
	}

	bc.AddTransaction(newTx) // Add the transaction to the mempool
	fmt.Println("All transactions: ", bc.GetTransactions())
	processedTransactionsMutex.Lock()
	processedTransactions[generateTransactionID(in)] = true
	processedTransactionsMutex.Unlock()

	if processedTransactions[generateTransactionID(in)] {
		return &pb.TransactionTokenResponse{Success: true}, nil
	}

	go func() {
		// call newbTransaction function to share the transaction with other miners
		//
	}() // this is a goroutine

	return &pb.TransactionTokenResponse{Success: true}, nil
}

func (s *server) TransferNFToken(ctx context.Context, in *pb.TransferNFTokenRequest) (*pb.TransferNFTokenResponse, error) {
	var txData interface{}
	var err error
	fmt.Println("Received new transaction: ", in)

	if !bc.CheckNFTId(in.GetTransaction().GetAddress(), int64(in.GetTransaction().GetId())) {
		fmt.Println("NFT ID does not exist")
		return &pb.TransferNFTokenResponse{Success: false}, fmt.Errorf("NFT ID does not exist")
	} else if bc.GetNFTOwner(in.GetTransaction().GetAddress(), int64(in.GetTransaction().GetId())) != in.GetTransaction().GetFrom() {
		fmt.Println("NFT ID does not belong to the sender")
		return &pb.TransferNFTokenResponse{Success: false}, fmt.Errorf("NFT ID does not belong to the sender")
	}
	txData = &bl.TransferNFToken{
		FROM:    in.GetTransaction().GetFrom(),
		TO:      in.GetTransaction().GetTo(),
		ADDRESS: in.GetTransaction().GetAddress(),
		ID:      int(in.GetTransaction().GetId()),
	}

	// Encode transaction data to JSON to calculate dataSize
	txDataBytes, err := json.Marshal(txData)
	if err != nil {
		return &pb.TransferNFTokenResponse{Success: false}, fmt.Errorf("error encoding transaction data: %v", err)
	}
	dataSize := len(txDataBytes)

	gasCost := bc.CalculateGas(dataSize, len(bc.GetTransactions()))

	fmt.Println("Gas cost: ", bc.ConvertWeiToEther(gasCost).String())

	newTx, err := bl.CreateTransaction(txData, in.GetSignature(), in.GetTxFrom())
	if err != nil {
		log.Println("Error creating transaction: ", err)
		return &pb.TransferNFTokenResponse{Success: false}, nil
	}

	verificationResult, err := verifyTransactionWithPython(txData, in.GetSignature(), in.GetTxFrom())
	if err != nil {
		log.Println("Error verifying transaction: ", err)
		return &pb.TransferNFTokenResponse{Success: false}, nil
	}

	if !verificationResult.Valid {
		return &pb.TransferNFTokenResponse{Success: false}, fmt.Errorf("transaction verification failed: %v", verificationResult.Error)
	}

	bc.AddTransaction(newTx) // Add the transaction to the mempool
	processedTransactionsMutex.Lock()
	processedTransactions[generateTransactionID(in)] = true
	processedTransactionsMutex.Unlock()

	if processedTransactions[generateTransactionID(in)] {
		return &pb.TransferNFTokenResponse{Success: true}, nil
	}

	return &pb.TransferNFTokenResponse{Success: true}, nil
}

func (s *server) BurnToken(ctx context.Context, in *pb.BurnTokenRequest) (*pb.BurnTokenResponse, error) {
	var txData interface{}
	var err error
	fmt.Println("Received new transaction: ", in)

	if in.GetTransaction().GetAmount() < 0 {
		log.Println("Amount cannot be negative")
		return &pb.BurnTokenResponse{Success: false}, fmt.Errorf("amount cannot be negative")
	} else if bc.GetTokenBalance(in.GetTransaction().GetFrom(), in.GetTransaction().GetAddress()) < int(in.GetTransaction().GetAmount()) {
		log.Println("Insufficient funds")
		return &pb.BurnTokenResponse{Success: false}, fmt.Errorf("insufficient funds")
	}
	txData = &bl.TransactionToken{
		FROM:    in.GetTransaction().GetFrom(),
		TO:      "0x0000000000000000000000000000000000000000",
		AMOUNT:  int(in.GetTransaction().GetAmount()),
		ADDRESS: in.GetTransaction().GetAddress(),
	}

	// Encode transaction data to JSON to calculate dataSize
	txDataBytes, err := json.Marshal(txData)
	if err != nil {
		return &pb.BurnTokenResponse{Success: false}, fmt.Errorf("error encoding transaction data: %v", err)
	}
	dataSize := len(txDataBytes)

	gasCost := bc.CalculateGas(dataSize, len(bc.GetTransactions()))

	fmt.Println("Gas cost: ", bc.ConvertWeiToEther(gasCost).String())

	newTx, err := bl.CreateTransaction(txData, in.GetSignature(), in.GetTxFrom())
	if err != nil {
		log.Println("Error creating transaction: ", err)
		return &pb.BurnTokenResponse{Success: false}, nil
	}

	verificationResult, err := verifyTransactionWithPython(txData, in.GetSignature(), in.GetTxFrom())
	if err != nil {
		log.Println("Error verifying transaction: ", err)
		return &pb.BurnTokenResponse{Success: false}, nil
	}

	if !verificationResult.Valid {
		return &pb.BurnTokenResponse{Success: false}, fmt.Errorf("transaction verification failed: %v", verificationResult.Error)
	}

	bc.AddTransaction(newTx) // Add the transaction to the mempool
	fmt.Println("All transactions: ", bc.GetTransactions())
	processedTransactionsMutex.Lock()
	processedTransactions[generateTransactionID(in)] = true
	processedTransactionsMutex.Unlock()

	if processedTransactions[generateTransactionID(in)] {
		return &pb.BurnTokenResponse{Success: true}, nil
	}

	go func() {
		// call newbTransaction function to share the transaction with other miners
		//
	}() // this is a goroutine

	return &pb.BurnTokenResponse{Success: true}, nil
}

func (s *server) BurnNFToken(ctx context.Context, in *pb.BurnNFTokenRequest) (*pb.BurnNFTokenResponse, error) {
	var txData interface{}
	var err error
	fmt.Println("Received new transaction: ", in)

	if !bc.CheckNFTId(in.GetTransaction().GetAddress(), int64(in.GetTransaction().GetId())) {
		fmt.Println("NFT ID does not exist")
		return &pb.BurnNFTokenResponse{Success: false}, fmt.Errorf("NFT ID does not exist")
	} else if bc.GetNFTOwner(in.GetTransaction().GetAddress(), int64(in.GetTransaction().GetId())) != in.GetTransaction().GetFrom() {
		fmt.Println("NFT ID does not belong to the sender")
		return &pb.BurnNFTokenResponse{Success: false}, fmt.Errorf("NFT ID does not belong to the sender")
	}
	txData = &bl.TransferNFToken{
		FROM:    in.GetTransaction().GetFrom(),
		TO:      "0x0000000000000000000000000000000000000000",
		ADDRESS: in.GetTransaction().GetAddress(),
		ID:      int(in.GetTransaction().GetId()),
	}

	// Encode transaction data to JSON to calculate dataSize
	txDataBytes, err := json.Marshal(txData)
	if err != nil {
		return &pb.BurnNFTokenResponse{Success: false}, fmt.Errorf("error encoding transaction data: %v", err)
	}
	dataSize := len(txDataBytes)

	gasCost := bc.CalculateGas(dataSize, len(bc.GetTransactions()))

	fmt.Println("Gas cost: ", bc.ConvertWeiToEther(gasCost).String())

	newTx, err := bl.CreateTransaction(txData, in.GetSignature(), in.GetTxFrom())
	if err != nil {
		log.Println("Error creating transaction: ", err)
		return &pb.BurnNFTokenResponse{Success: false}, nil
	}

	verificationResult, err := verifyTransactionWithPython(txData, in.GetSignature(), in.GetTxFrom())
	if err != nil {
		log.Println("Error verifying transaction: ", err)
		return &pb.BurnNFTokenResponse{Success: false}, nil
	}

	if !verificationResult.Valid {
		return &pb.BurnNFTokenResponse{Success: false}, fmt.Errorf("transaction verification failed: %v", verificationResult.Error)
	}

	bc.AddTransaction(newTx) // Add the transaction to the mempool
	fmt.Println("All transactions: ", bc.GetTransactions())
	processedTransactionsMutex.Lock()
	processedTransactions[generateTransactionID(in)] = true
	processedTransactionsMutex.Unlock()

	if processedTransactions[generateTransactionID(in)] {
		return &pb.BurnNFTokenResponse{Success: true}, nil
	}

	go func() {
		// call newbTransaction function to share the transaction with other miners
		//
	}() // this is a goroutine

	return &pb.BurnNFTokenResponse{Success: true}, nil
}

func (s *server) NewBlock(ctx context.Context, in *pb.NewBlockRequest) (*pb.NewBlockResponse, error) {
	// get block from request
	var transactions []*bl.Transaction
	for _, tx := range in.Block.Transactions {
		var transaction *bl.Transaction
		var err error

		switch txData := tx.Data.TxData.(type) {
		case *pb.InfoTransaction_DefaultTx:
			transaction, err = bl.CreateTransaction(&bl.DefaultTransaction{
				FROM:   txData.DefaultTx.From,
				TO:     txData.DefaultTx.To,
				AMOUNT: int(txData.DefaultTx.Amount),
			}, tx.GetData().GetSignature(), transaction.FROM)
		case *pb.InfoTransaction_CreateTokenTx:
			transaction, err = bl.CreateTransaction(&bl.CreateToken{
				NAME:    txData.CreateTokenTx.Name,
				SYMBOL:  txData.CreateTokenTx.Symbol,
				SUPPLY:  int(txData.CreateTokenTx.Supply),
				ADDRESS: txData.CreateTokenTx.Address,
				OWNER:   txData.CreateTokenTx.Owner,
			}, tx.Data.Signature, transaction.FROM)
		case *pb.InfoTransaction_CreateNfTokenTx:
			transaction, err = bl.CreateTransaction(&bl.CreateNFToken{
				NAME:    txData.CreateNfTokenTx.Name,
				SYMBOL:  txData.CreateNfTokenTx.Symbol,
				OWNER:   txData.CreateNfTokenTx.Owner,
				ADDRESS: txData.CreateNfTokenTx.Address,
			}, tx.Data.Signature, transaction.FROM)
		case *pb.InfoTransaction_MintNfTokenTx:
			transaction, err = bl.CreateTransaction(&bl.MintNFToken{
				TO:      txData.MintNfTokenTx.To,
				ID:      int(txData.MintNfTokenTx.Id),
				ADDRESS: txData.MintNfTokenTx.Address,
			}, tx.Data.Signature, transaction.FROM)
		case *pb.InfoTransaction_TransactionTokenTx:
			transaction, err = bl.CreateTransaction(&bl.TransactionToken{
				FROM:    txData.TransactionTokenTx.From,
				TO:      txData.TransactionTokenTx.To,
				AMOUNT:  int(txData.TransactionTokenTx.Amount),
				ADDRESS: txData.TransactionTokenTx.Address,
			}, tx.Data.Signature, transaction.FROM)
		case *pb.InfoTransaction_TransferNfTokenTx:
			transaction, err = bl.CreateTransaction(&bl.TransferNFToken{
				FROM:    txData.TransferNfTokenTx.From,
				TO:      txData.TransferNfTokenTx.To,
				ADDRESS: txData.TransferNfTokenTx.Address,
				ID:      int(txData.TransferNfTokenTx.Id),
			}, tx.Data.Signature, transaction.FROM)
		case *pb.InfoTransaction_BurnTokenTx:
			transaction, err = bl.CreateTransaction(&bl.BurnToken{
				FROM:    txData.BurnTokenTx.From,
				ADDRESS: txData.BurnTokenTx.Address,
				AMOUNT:  int(txData.BurnTokenTx.Amount),
			}, tx.Data.Signature, transaction.FROM)
		case *pb.InfoTransaction_BurnNfTokenTx:
			transaction, err = bl.CreateTransaction(&bl.BurnNFToken{
				FROM:    txData.BurnNfTokenTx.From,
				ADDRESS: txData.BurnNfTokenTx.Address,
				ID:      int(txData.BurnNfTokenTx.Id),
			}, tx.Data.Signature, transaction.FROM)
		default:
			err = fmt.Errorf("unknown transaction type")
		}

		if err != nil {
			log.Println("Error creating transaction: ", err)
			return &pb.NewBlockResponse{Success: false}, nil
		}
		transactions = append(transactions, transaction)
	}

	log.Println("Received new block: ", in.Block)

	block := &bl.Block{
		Index:        int(in.Block.Index),
		Timestamp:    in.Block.Timestamp,
		Transactions: transactions,
		PrevHash:     in.Block.PreviousHash,
		MerkleRoot:   in.Block.MerkleRoot,
		Nonce:        int(in.Block.Nonce),
		Hash:         in.Block.Hash,
		Miner:        in.Block.Miner,
		Difficulty:   int(in.Block.Difficulty),
	}

	if bc.VerifyBlock(block) {
		bc.AddBlock(block)
		return &pb.NewBlockResponse{Success: true}, nil
	} else {
		// remove the miner owner of the block from the miner list
		mn.RemoveMiner(in.Block.Miner)
		return &pb.NewBlockResponse{Success: false}, nil
	}
	return &pb.NewBlockResponse{Success: false}, nil
}

func (s *server) GetBalance(ctx context.Context, in *pb.GetBalanceRequest) (*pb.GetBalanceResponse, error) {
	balance := bc.GetBalance(in.GetAddress())
	return &pb.GetBalanceResponse{Balance: int64(balance)}, nil
}

func (s *server) GetTokenBalance(ctx context.Context, in *pb.GetTokenBalanceRequest) (*pb.GetTokenBalanceResponse, error) {
	balance := bc.GetTokenBalance(in.GetWallet(), in.GetTokenAddress())
	fmt.Println("Balance: ", balance)
	return &pb.GetTokenBalanceResponse{Balance: int64(balance)}, nil
}

func (s *server) GetNFTBalance(ctx context.Context, in *pb.GetNFTBalanceRequest) (*pb.GetNFTBalanceResponse, error) {
	fmt.Println("Received: ", in.GetWallet(), in.GetTokenAddress())
	listId := bc.GetNFTBalance(in.GetWallet(), in.GetTokenAddress())
	return &pb.GetNFTBalanceResponse{NftIds: listId}, nil
}

func (s *server) GetNFTOwner(ctx context.Context, in *pb.GetNFTOwnerRequest) (*pb.GetNFTOwnerResponse, error) {
	owner := bc.GetNFTOwner(in.GetTokenAddress(), in.NftId)
	return &pb.GetNFTOwnerResponse{Owner: owner}, nil
}

func (s *server) GetTokenInfo(ctx context.Context, in *pb.GetTokenInfoRequest) (*pb.GetTokenInfoResponse, error) {
	info := bc.GetTokenInfo(in.GetTokenAddress())
	return &pb.GetTokenInfoResponse{
		Name:   info.NAME,
		Symbol: info.SYMBOL,
		Supply: int64(info.SUPPLY),
		Owner:  info.OWNER,
	}, nil
}

func (s *server) GetNFTInfo(ctx context.Context, in *pb.GetNFTInfoRequest) (*pb.GetNFTInfoResponse, error) {
	info := bc.GetNFTInfo(in.GetTokenAddress())
	return &pb.GetNFTInfoResponse{
		Name:   info.NAME,
		Symbol: info.SYMBOL,
		Owner:  info.OWNER,
	}, nil
}

func (s *server) GetBlocksByPage(ctx context.Context, in *pb.GetBlocksByPageRequest) (*pb.GetBlocksByPageResponse, error) {
	fmt.Println("Received: ", in.GetPageNumber(), in.GetPageSize())

	blocks, err := bc.GetBlocksByPage(int(in.GetPageNumber()), int(in.GetPageSize()))
	if err != nil {
		return &pb.GetBlocksByPageResponse{}, err
	}

	var pbBlocks []*pb.Block
	for _, block := range blocks {
		pbBlock := &pb.Block{
			Index:        int64(block.Index),
			Timestamp:    block.Timestamp,
			Transactions: []*pb.Transaction{},
			PreviousHash: block.PrevHash,
			MerkleRoot:   block.MerkleRoot,
			Nonce:        int64(block.Nonce),
			Hash:         block.Hash,
			Miner:        block.Miner,
			Difficulty:   int64(block.Difficulty),
		}
		pbBlocks = append(pbBlocks, pbBlock)
	}

	return &pb.GetBlocksByPageResponse{Blocks: pbBlocks}, nil
}

func (s *server) GetBlock(ctx context.Context, req *pb.GetBlockRequest) (*httpbody.HttpBody, error) {
	block, err := bl.GetBlockByIndex(int(req.GetIndex()))
	if err != nil {
		return nil, err
	}

	blockStruct := map[string]interface{}{
		"index":        block.Index,
		"timestamp":    block.Timestamp,
		"transactions": block.Transactions,
		"previousHash": block.PrevHash,
		"merkleRoot":   block.MerkleRoot,
		"nonce":        block.Nonce,
		"hash":         block.Hash,
		"miner":        block.Miner,
		"difficulty":   block.Difficulty,
	}

	blockJSON, err := json.Marshal(blockStruct)
	if err != nil {
		return nil, err
	}

	return &httpbody.HttpBody{
		ContentType: "application/json",
		Data:        blockJSON,
	}, nil

}

func GRPC() {
	lis, err := net.Listen("tcp", ":3000")
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}
	s := grpc.NewServer()
	pb.RegisterNodeServiceServer(s, &server{})
	log.Println("Server is running on port 3000")

	if err := s.Serve(lis); err != nil {
		log.Fatalf("failed to serve: %v", err)
	}
}
