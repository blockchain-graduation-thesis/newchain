# GChain

## Requirements
- golang go1.22.2

## How to Run

### Run with golang on Host Machine

1. **Setup Go Environment**
    - Ensure you have Go 1.22.2 installed.

2. **Install Dependencies**
    - Install the required dependencies using `go mod tidy`:
      ```bash
      go mod tidy
      ```

3. **Run the Application**
    - Execute the main script using:
      ```bash
      go run main.go
      ```

4. **Access the Application**
    - Follow the instructions displayed in the console to access and interact with the application.

## Output
The output service runs on `http://54.169.241.252:8000`
